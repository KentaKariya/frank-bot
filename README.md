# frank-bot
Frank is a trip planner Discord bot for the public transportation network in the Rhein-Main region/Frankfurt metropolitan area (RMV).

### Search for stations
![Search](doc/images/search.png)

### List upcoming departures
![Departures](doc/images/departures.png)

### Show trip options
![Trip search](doc/images/trip_search.png)

### Get trip details
![Trip details](doc/images/trip_details.png)
