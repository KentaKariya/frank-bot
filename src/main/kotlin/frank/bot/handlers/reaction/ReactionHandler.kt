package frank.bot.handlers.reaction

import discord4j.core.event.domain.message.ReactionAddEvent

/**
 * Handles emoji reactions to a message.
 */
abstract class ReactionHandler {

    /**
     * Handles all relevant message reactions.
     */
    fun handle(event: ReactionAddEvent) {
        if(isValidReaction(event)) processReaction(event)
    }

    protected abstract fun processReaction(event: ReactionAddEvent)

    protected abstract fun isValidReaction(event: ReactionAddEvent) : Boolean

}
