package frank.bot.handlers.command

import discord4j.core.`object`.entity.channel.MessageChannel
import frank.api.Requester
import frank.api.response.LocationResponse
import frank.api.service.LocationService
import frank.commandPrefix
import frank.util.apiEmbedTemplate
import retrofit2.Call

/**
 * Handles "search" commands to list stations matching a query string.
 */
class LocationHandler(private val locationService: LocationService) : CommandHandler<LocationResponse>() {

    private var query: String = ""

    /**
     * Creates an API requests from user-given arguments.
     */
    override fun createRequest(args: List<String>): Call<LocationResponse> {
        query = args.subList(2, args.size).joinToString(" ")
        return locationService.getStationsByName(query)
    }

    /**
     * Processes the received response from the API.
     */
    override fun processResponse(requester: Requester, channel: MessageChannel, response: LocationResponse) {
        channel.createMessage { msg -> msg.setEmbed(getEmbed(query, response)) }.subscribe()
    }

    /**
     * Creates an message embed containing up to nine stations.
     */
    private fun getEmbed(query: String, response: LocationResponse) = apiEmbedTemplate.andThen { spec ->
        spec.setTitle("Location results for $query")
        response.locationWrappers.take(9).map { w -> w.location }.forEach {
            spec.addField(it.name, it.extId, true)
        }
    }

    /**
     * Validates user-given arguments by checking its count.
     */
    override fun isValidRequest(args: List<String>) = args.size > 2

    /**
     * Command usage for help message
     */
    override fun usage() = "$commandPrefix search <query>"

}
