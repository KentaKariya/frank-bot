package frank.util

import discord4j.core.`object`.entity.Message
import discord4j.core.spec.EmbedCreateSpec
import discord4j.rest.util.Color
import java.util.function.Consumer

/**
 * Splits a message into command arguments.
 */
fun Message.getCommandArgs() = content.split(" ")

/**
 * Valid reactions emojis (number emojis 1 to 5)
 */
val reactionEmojis = listOf('\u0031', '\u0032', '\u0033', '\u0034', '\u0035').map { c -> "$c\u20e3" }

/**
 * Standard template for all embeds
 */
val embedTemplate = Consumer<EmbedCreateSpec> { spec -> spec.setColor(Color.DEEP_LILAC) }

/**
 * Standard template for all API response embeds to be compliant with API TOS
 */
val apiEmbedTemplate: Consumer<EmbedCreateSpec> = embedTemplate.andThen { spec ->
    spec.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Rhein-Main-Verkehrsverbund_logo.svg/1920px-Rhein-Main-Verkehrsverbund_logo.svg.png")
    spec.setFooter("Source: Rhein-Main-Verkehrsverbund", null)
}
