package frank.util

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

/**
 * API default date format
 */
fun getDateFormat() = DateTimeFormat.forPattern("yyyy-MM-dd")

/**
 * API default time format
 */
fun getTimeFormat(): DateTimeFormatter = DateTimeFormat.forPattern("HH:mm")

/**
 * API default datetime format
 */
fun getDateTime(date: String, time: String) : DateTime = DateTime.parse(
    date + time, DateTimeFormat.forPattern("yyyy-MM-ddHH:mm:ss")
)

/**
 * Returns a nullable [DateTime] instance from nullable date and time strings.
 */
fun getNullableDateTime(date: String?, time: String?) : DateTime? {
    return if(date != null && time != null) getDateTime(date, time) else null
}

/**
 * Extension function for [DateTime] to convert instance into time string
 */
fun DateTime.getDateTimeString() = toString(getTimeFormat())

/**
 * Checks if given string is a extId using a regex pattern.
 */
fun isExtId(extId: String) = extId.matches(Regex("[0-9]{7}"))
