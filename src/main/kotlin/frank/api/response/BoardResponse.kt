package frank.api.response

import com.google.gson.annotations.SerializedName
import frank.api.entities.board.BoardElement

class BoardResponse(
    @SerializedName("Departure") val elements: List<BoardElement>
) : Response {

    fun getStation() = elements[0].stop

    override fun isValid() = elements.isNotEmpty()

}
