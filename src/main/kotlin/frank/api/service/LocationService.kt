package frank.api.service

import frank.api.response.LocationResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Interface for API requests to the "location.*" endpoint.
 */
interface LocationService {

    /**
     * Retrieves a list of locations matching a given string,
     * limiting results to stations.
     */
    @GET("location.name")
    fun getStationsByName(
        @Query("input") input: String,
        @Query("type") type: String = "S"
    ) : Call<LocationResponse>

}
