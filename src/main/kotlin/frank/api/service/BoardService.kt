package frank.api.service

import frank.api.response.BoardResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Interface for API requests to the "arrivalBoard"/"departureBoard" endpoint.
 */
interface BoardService {

    /**
     * Retrieves a list of departures from the given station
     * with a limit on the number of results.
     */
    @GET("departureBoard")
    fun getDepartures(
        @Query("extId") extId: String,
        @Query("maxJourneys") maxJourneys: Int = 5
    ) : Call<BoardResponse>

}
